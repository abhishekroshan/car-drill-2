function findCarWithId(inventory, Id) {
  if (!Array.isArray(inventory) || typeof Id !== "number") {
    console.log("Inventory is not an Array Or id is not a number");
    return null;
  }
  //checking if inventory is array or not or id is number or not

  const carSearch = inventory.filter((car) => car.id === Id);
  //storing the details of that perticular id

  if (carSearch.length > 0) {
    const car = carSearch[0];
    return `Car ${Id} is a ${car.car_year} ${car.car_make} ${car.car_model}`;
  } else {
    console.log(`No car found`);
    return null;
  }
}

module.exports = findCarWithId;
