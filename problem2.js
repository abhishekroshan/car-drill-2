function lastCarInInventory(inventory, Id) {
  if (!Array.isArray(inventory)) {
    console.log("Inventory is not an Array");
    return null;
  }
  //checking if inventory is array or not or id is number or not

  const lastCar = inventory.filter((car) => car.id === inventory.length);
  //storing the details of last car

  if (lastCar.length > 0) {
    const car = lastCar[0];
    return `Last Car is a ${car.car_make} ${car.car_model}`;
  } else {
    console.log(`Inventory is empty`);
    return null;
  }
}

module.exports = lastCarInInventory;
