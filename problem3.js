function carsAfterSorting(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("inventory is not an array");
    return null;
  }
  //checking if the inventory is array or not.

  const sortedInventory = inventory.map((car) => car.car_model);
  //pushing the car models of the inventory in the variable sortedInventory;

  sortedInventory.sort();
  //sorting the Inventory

  return sortedInventory;
}

module.exports = carsAfterSorting;
