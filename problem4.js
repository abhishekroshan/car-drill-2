function carByYears(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("Inventory is not an array");
    return null;
  }
  //checking if the inventory is array or not

  const manufacturingYears = inventory.map((years) => years.car_year);
  //using map to push the car_year from the inventory in a seperate variable.

  return manufacturingYears;
}

module.exports = carByYears;
