function oldCarArray(arrayOfCarYears) {
  if (!Array.isArray(arrayOfCarYears)) {
    console.log("The parameter is not an array");
    return null;
  }

  const olderCars = arrayOfCarYears.filter((old) => {
    return old < 2000;
  });
  //we are passing an array of years only & this filters
  //the car years which are older than 2000

  return olderCars;
}

module.exports = oldCarArray;
