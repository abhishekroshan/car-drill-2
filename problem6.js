function bmwAndAudiOnly(inventory) {
  if (!Array.isArray(inventory)) {
    console.log("Inventory is not an array");
    return null;
  }
  //checking if inventory is array or not

  const filterBmwAndAudi = inventory.filter(
    (car) => car.car_make === "BMW" || car.car_make === "Audi"
  );
  //using filter method to filter bmw and audi cars from the inventory

  const bmwAndAudi = filterBmwAndAudi.map((cars) => ({
    maker: cars.car_make,
    model: cars.car_model,
  }));
  //using map to store the car_make and car_model after filter.

  return bmwAndAudi;
}

module.exports = bmwAndAudiOnly;
