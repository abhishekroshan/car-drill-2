const carsAfterSorting = require("./problem3");
const inventory = require("./inventory");

const result = carsAfterSorting(inventory);

if (result.length !== 0) {
  console.log(result);
} else {
  console.log("Inventory is empty");
}
