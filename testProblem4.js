const carByYears = require("./problem4");
const inventory = require("./inventory");

const result = carByYears(inventory);

if (result.length !== 0) {
  console.log(result);
} else {
  console.log("Inventory is empty");
}
