const carByYears = require("./problem4");
const inventory = require("./inventory");
const oldCarArray = require("./problem5");

const allCars = carByYears(inventory);

const oldCars = oldCarArray(allCars);

if (oldCars.length > 0) {
  console.log(oldCars);
  console.log(oldCars.length);
} else {
  console.log("No cars older than 2000 found");
}
