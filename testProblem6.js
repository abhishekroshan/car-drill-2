const bmwAndAudiOnly = require("./problem6");
const inventory = require("./inventory");

const result = bmwAndAudiOnly(inventory);

if (result.length > 0) {
  console.log(JSON.stringify(result));
} else {
  console.log("No BMW and Audi Cars Found");
}
